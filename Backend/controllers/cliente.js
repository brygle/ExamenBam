const { response } = require('express');
const { getConnection, sql } = require('../database/connection');


const crearCliente = async (req, res = response) => {
    const { nombre1, nombre2, apellido1, apellido2, apellido_casada, direccion, telefono1, telefono2, identificacion, fecha_nacimiento } = req.body;
    if (nombre1 == null || apellido1 == null || direccion == null || identificacion == null || fecha_nacimiento == null) {
        return res.status(400).json({ ok: false, msg: "Los campos: nombre1, apellido1, direccion, identificacion, fecha_nacimiento son obligatorios" });
    }
    try {
        const pool = await getConnection();
        await pool
            .request()
            .input("nombre1", sql.VarChar, nombre1)
            .input("nombre2", sql.VarChar, nombre2)
            .input("apellido1", sql.VarChar, apellido1)
            .input("apellido2", sql.VarChar, apellido2)
            .input("apellido_casada", sql.VarChar, apellido_casada)
            .input("direccion", sql.VarChar, direccion)
            .input("telefono1", sql.Int, telefono1)
            .input("telefono2", sql.Int, telefono2)
            .input("identificacion", sql.VarChar, identificacion)
            .input("fecha_nacimiento", sql.SmallDateTime, fecha_nacimiento)
            .query("INSERT INTO Cliente (cli_nombre1, cli_nombre2, cli_apellido1, cli_apellido2, cli_apellido_casada, cli_direccion, cli_telefono1, cli_telefono2, cli_identificacion, cli_fecha_nacimiento) VALUES (@nombre1, @nombre2, @apellido1, @apellido2, @apellido_casada, @direccion, @telefono1, @telefono2, @identificacion, @fecha_nacimiento);");
        res.status(200);
        res.send({ msg: "Cliente ingresado correctamente" });
    } catch (error) {
        res.status(500);
        res.send(error.message);
    }
    res.json({
        ok: true,
        msg: 'crear cliente'
    });
}

const verCliente = async (req, res = response) => {
    try {
        const pool = await getConnection();

        const result = await pool
            .request()
            .input("codigo", req.params.codigo)
            .query("SELECT * FROM Cliente Where cli_codigo_cliente = @codigo");
        return res.status(200).json(result.recordset[0]);
    } catch (error) {
        res.status(500);
        res.send(error.message);
    }
}

const verClientes = async (req, res = response) => {

    const pool = await getConnection();
    const result = await pool.request().query('SELECT * FROM Cliente');
    console.log(result)
    res.json({
        ok: true,
        clientes: result.recordset
    });
}

const actualizarCliente = async (req, res = response) => {
    try {
        const pool = await getConnection();
        const {direccion, telefono1} = req.body;
        await pool
            .request()
            .input("codigo", req.params.codigo)
            .input("direccion", sql.VarChar, direccion)
            .input("telefono1", sql.Int, telefono1)
            .query("UPDATE Cliente SET cli_direccion=@direccion, cli_telefono1 = @telefono1 Where cli_codigo_cliente = @codigo");
        return res.status(200).json({ok: true, msg: "Cliente actualizado correctamente"});
    } catch (error) {
        res.status(500);
        res.send(error.message);
    }
}

const eliminarCliente = async (req, res = response) => {
    try {
        const pool = await getConnection();
        const result = await pool
            .request()
            .input("codigo", req.params.codigo)
            .query("DELETE FROM Cliente Where cli_codigo_cliente = @codigo");
        return res.status(200).json({ok: true, msg:"Cliente eliminado exitosamente"});
    } catch (error) {
        res.status(500);
        res.send(error.message);
    }
}

module.exports = {
    crearCliente,
    verCliente,
    verClientes,
    actualizarCliente,
    eliminarCliente
}