const sql = require('mssql');

const dbSettings = {
    user: 'bryan',
    password: '1234',
    server: 'localhost',
    database: 'SEExamen2021',
    options : {
        encrypt: true,
        trustServerCertificate: true,
    }
};

async function getConnection() {
    try {
        const pool = await sql.connect(dbSettings);
        return pool;    
    } catch (error) {
        console.error(error);
    }
    
}
module.exports = {getConnection, sql};