const express = require('express');
require('dotenv').config();
const cors = require('cors');

//Crear el servidor de express
const app = express();

//CORS
app.use(cors())
app.use(express.json());
app.use(express.urlencoded({extended: false}));

//Variable de entorno
const port = process.env.PORT;

//Rutas 
//CRUD: Clientes
app.use('/api/cliente', require('./routes/cliente'));

// Levantar servidor
app.listen(port, ()=> {
    console.log(`Servidor corriendo en puerto ${port}`);
});
