/*
    Rutas de clientes / cliente
    host + /api/cliente
*/
const { Router } = require('express');
const router = Router();
const { crearCliente, verCliente, verClientes, actualizarCliente, eliminarCliente } = require('../controllers/cliente')

router.post('/crearCliente', crearCliente);

router.get('/verCliente/:codigo', verCliente);

router.get('/verClientes', verClientes);

router.put('/actualizarCliente/:codigo', actualizarCliente);

router.delete('/eliminarCliente/:codigo', eliminarCliente);

module.exports = router;