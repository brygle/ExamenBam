import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom';
import { getClientes } from '../../selectors/getClientes';

export const ListaReportes = () => {

    const [clientes, setClientes] = useState([]);

    const cargar = async (car) => {
        const {clientes } = await getClientes(car);
        console.log('clientes');
        setClientes(clientes);
        console.log(clientes);
    }

    useEffect(() => {
        cargar();
    }, []);
    

    return (
        <>
            <table className="table table-dark table-striped">
                <thead>
                    <tr>
                        <th scope="col">Codigo</th>
                        <th scope="col">Nombre 1</th>
                        <th scope="col">Nombre 2</th>
                        <th scope="col">Apellido 1</th>
                        <th scope="col">Apellido 2</th>
                        <th scope="col">Apellido Casada</th>
                        <th scope="col">Direccion</th>
                        <th scope="col">Telefono 1</th>
                        <th scope="col">Telefono 2</th>
                        <th scope="col">Identificacion</th>
                        <th scope="col">Fecha de nacimiento</th>
                        <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>
                    {
                        clientes.map(cliente => (
                            <tr key={cliente.cli_codigo_cliente}>
                                <td>{cliente.cli_codigo_cliente}</td>
                                <td>{cliente.cli_nombre1}</td>
                                <td>{cliente.cli_nombre2}</td>
                                <td>{cliente.cli_apellido1}</td>
                                <td>{cliente.cli_apellido2}</td>
                                <td>{cliente.cli_apellido_casada}</td>
                                <td>{cliente.cli_direccion}</td>
                                <td>{cliente.cli_telefono1}</td>
                                <td>{cliente.cli_telefono2}</td>
                                <td>{cliente.cli_identificacion}</td>
                                <td>{cliente.cli_fecha_nacimiento.substring(0, 10)}</td>
                                <td> <Link className="link-light" to={`./reporte/${cliente.cli_codigo_cliente}`}> Ver </Link> </td>
                            </tr>
                        ))
                    }
                </tbody>
            </table>
        </>

    )
}
