import React, { useState } from 'react'
import { createCliente } from '../../selectors/createCliente';
import { postReporte } from '../../selectors/postReporte';

export const RegistrarReporte = () => {

    const [carnet, setCarnet] = useState('');
    const [nombre, setNombre] = useState('');
    const [curso, setCurso] = useState('');
    const [reporte, setReporte] = useState('');

    const [nombre1, setNombre1] = useState('');
    const [nombre2, setNombre2] = useState('');
    const [apellido1, setApellido1] = useState('');
    const [apellido2, setApellido2] = useState('');
    const [apellido_casada, setApellido_casada] = useState('');
    const [direccion, setDireccion] = useState('');
    const [telefono1, setTelefono1] = useState('');
    const [telefono2, setTelefono2] = useState('');
    const [identificacion, setIdentificacion] = useState('');
    const [fecha_nacimiento, setFecha_nacimiento] = useState('');

    const handleSubmit = async (e) => {
        e.preventDefault();
        const respuesta = await createCliente(nombre1, nombre2, apellido1, apellido2, apellido_casada, direccion, telefono1, telefono2, identificacion, fecha_nacimiento);
        console.log(respuesta);
        alert('Cliente registrado correctamente!!!');
        limpiar();
    }

    const nombre1Change = (e) => {
        setNombre1(e.target.value);
    }

    const nombre2Change = (e) => {
        setNombre2(e.target.value);
    }

    const apellido1Change = (e) => {
        setApellido1(e.target.value);
    }

    const apellido2Change = (e) => {
        setApellido2(e.target.value);
    }

    const apellido_casadaChange = (e) => {
        setApellido_casada(e.target.value);
    }

    const direccionChange = (e) => {
        setDireccion(e.target.value);
    }

    const telefono1Change = (e) => {
        setTelefono1(e.target.value);
    }

    const telefono2Change = (e) => {
        setTelefono2(e.target.value);
    }

    const identificacionChange = (e) => {
        setIdentificacion(e.target.value);
    }

    const fecha_nacimientoChange = (e) => {
        setFecha_nacimiento(e.target.value);
    }


    const limpiar = () => {
        setNombre1('');
        setNombre2('');
        setApellido1('');
        setApellido2('');
        setApellido_casada('');
        setDireccion('');
        setTelefono1('');
        setTelefono2('');
        setIdentificacion('');
        setFecha_nacimiento('');
    }

    return (
        <div>
            <form onSubmit={ handleSubmit }>
                <legend>Crear Cliente</legend>
                <div className="mb-3">
                    <label htmlFor="Nombre1TextInput" className="form-label">Nombre 1</label>
                    <input type="text" id="Nombre1TextInput" className="form-control" placeholder="Nombre 1" defaultValue={nombre1} onChange={nombre1Change}/>
                </div>
                <div className="mb-3">
                    <label htmlFor="Nombre2TextInput" className="form-label">Nombre 2</label>
                    <input type="text" id="Nombre2TextInput" className="form-control" placeholder="Nombre 2" defaultValue={nombre2} onChange={nombre2Change}/>
                </div>
                <div className="mb-3">
                    <label htmlFor="Apellido1TextInput" className="form-label">Apellido 1</label>
                    <input type="text" id="Apellido1TextInput" className="form-control" placeholder="Apellido 1" defaultValue={apellido1} onChange={apellido1Change} />
                </div>
                <div className="mb-3">
                    <label htmlFor="Apellido2TextInput" className="form-label">Apellido 2</label>
                    <input type="text" id="Apellido2TextInput" className="form-control" placeholder="Apellido 2" defaultValue={apellido2} onChange={apellido2Change} />
                </div>
                <div className="mb-3">
                    <label htmlFor="ApellidoCasadaTextInput" className="form-label">Apellido Casada</label>
                    <input type="text" id="ApellidoCasadaTextInput" className="form-control" placeholder="Apellido de casada" defaultValue={apellido_casada} onChange={apellido_casadaChange}/>
                </div>
                <div className="mb-3">
                    <label htmlFor="direccionTextInput" className="form-label">Direccion</label>
                    <input type="text" id="direccionTextInput" className="form-control" placeholder="Direccion" defaultValue={direccion} onChange={direccionChange} />
                </div>
                <div className="mb-3">
                    <label htmlFor="telefono1TextInput" className="form-label">Telefono 1</label>
                    <input type="text" id="telefono1TextInput" className="form-control" placeholder="Telefono 1" defaultValue={telefono1} onChange={telefono1Change} />
                </div>
                <div className="mb-3">
                    <label htmlFor="telefono2TextInput" className="form-label">Telefono 2</label>
                    <input type="text" id="telefono2TextInput" className="form-control" placeholder="Telefono 2" defaultValue={telefono2} onChange={telefono2Change} />
                </div>
                <div className="mb-3">
                    <label htmlFor="identificacionTextInput" className="form-label">Identificacion</label>
                    <input type="text" id="identificacionTextInput" className="form-control" placeholder="Identificacion" defaultValue={identificacion}  onChange={identificacionChange}/>
                </div>
                <div className="mb-3">
                    <label htmlFor="fecha_nacimientoTextarea" className="form-label">Fecha de Nacimiento</label>
                    <textarea className="form-control" placeholder="Fecha de nacimiento" id="fecha_nacimientoTextarea" defaultValue={fecha_nacimiento.substring(0, 10)} onChange={fecha_nacimientoChange}></textarea>
                </div>
                <button className="btn btn-dark">Registrar</button>
            </form>
        </div >
    )
}
