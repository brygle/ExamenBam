import React, { useEffect, useState } from 'react'
import { Redirect, useParams } from 'react-router'
import { deleteClienteByCodigo } from '../../selectors/deleteClienteByCodigo';
import { updateClienteByCodigo } from '../../selectors/updateClienteByCodigo';
import { getClienteByCodigo } from '../../selectors/getClienteByCodigo';

export const ReporteIndividual = () => {

    const {codigo} = useParams();

    const [nombre1, setNombre1] = useState('');
    const [nombre2, setNombre2] = useState('');
    const [apellido1, setApellido1] = useState('');
    const [apellido2, setApellido2] = useState('');
    const [apellido_casada, setApellido_casada] = useState('');
    const [direccion, setDireccion] = useState('');
    const [telefono1, setTelefono1] = useState('');
    const [telefono2, setTelefono2] = useState('');
    const [identificacion, setIdentificacion] = useState('');
    const [fecha_nacimiento, setFecha_nacimiento] = useState('');

    const cargar = async (i) => {
        const { cli_nombre1,  cli_nombre2,  cli_apellido1,  cli_apellido2,  cli_apellido_casada,  cli_direccion,  cli_telefono1,  cli_telefono2,  cli_identificacion,  cli_fecha_nacimiento } = await getClienteByCodigo(i);
        setNombre1(cli_nombre1);
        setNombre2(cli_nombre2);
        setApellido1(cli_apellido1);
        setApellido2(cli_apellido2);
        setApellido_casada(cli_apellido_casada);
        setDireccion(cli_direccion);
        setTelefono1(cli_telefono1);
        setTelefono2(cli_telefono2);
        setIdentificacion(cli_identificacion);
        setFecha_nacimiento(cli_fecha_nacimiento);
    }

    useEffect(() => {
        cargar(codigo);
    }, []);

    const actualizarCliente = async(e) => {
        console.log(codigo);
        const respuesta = await updateClienteByCodigo(codigo, direccion, telefono1);
        alert('Cliente actualizado correctamente!!!');
    }

    const eliminarCliente = async(e) => {
        console.log(codigo);
        const respuesta = await deleteClienteByCodigo(codigo);
        alert('Cliente eliminado correctamente!!!');
        
    }

    const direccionChange = (e) => {
        setDireccion(e.target.value);
    }

    const telefono1Change = (e) => {
        setTelefono1(e.target.value);
    }

    return (
        <div>
            <form >
                <legend>Ver Cliente</legend>
                <div className="mb-3">
                    <label htmlFor="codigoTextInput" className="form-label">Codigo</label>
                    <input type="text" id="codigoTextInput" className="form-control" placeholder="Codigo" defaultValue={codigo} readOnly />
                </div>
                <div className="mb-3">
                    <label htmlFor="Nombre1TextInput" className="form-label">Nombre 1</label>
                    <input type="text" id="Nombre1TextInput" className="form-control" placeholder="Nombre 1" defaultValue={nombre1} readOnly />
                </div>
                <div className="mb-3">
                    <label htmlFor="Nombre2TextInput" className="form-label">Nombre 2</label>
                    <input type="text" id="Nombre2TextInput" className="form-control" placeholder="Nombre 2" defaultValue={nombre2} readOnly />
                </div>
                <div className="mb-3">
                    <label htmlFor="Apellido1TextInput" className="form-label">Apellido 1</label>
                    <input type="text" id="Apellido1TextInput" className="form-control" placeholder="Apellido 1" defaultValue={apellido1} readOnly />
                </div>
                <div className="mb-3">
                    <label htmlFor="Apellido2TextInput" className="form-label">Apellido 2</label>
                    <input type="text" id="Apellido2TextInput" className="form-control" placeholder="Apellido 2" defaultValue={apellido2} readOnly />
                </div>
                <div className="mb-3">
                    <label htmlFor="ApellidoCasadaTextInput" className="form-label">Apellido Casada</label>
                    <input type="text" id="ApellidoCasadaTextInput" className="form-control" placeholder="Apellido de casada" defaultValue={apellido_casada} readOnly />
                </div>
                <div className="mb-3">
                    <label htmlFor="direccionTextInput" className="form-label">Direccion</label>
                    <input type="text" id="direccionTextInput" className="form-control" placeholder="Direccion" defaultValue={direccion} onChange={direccionChange} />
                </div>
                <div className="mb-3">
                    <label htmlFor="telefono1TextInput" className="form-label">Telefono 1</label>
                    <input type="text" id="telefono1TextInput" className="form-control" placeholder="Telefono 1" defaultValue={telefono1} onChange={telefono1Change}/>
                </div>
                <div className="mb-3">
                    <label htmlFor="telefono2TextInput" className="form-label">Telefono 2</label>
                    <input type="text" id="telefono2TextInput" className="form-control" placeholder="Telefono 2" defaultValue={telefono2} readOnly />
                </div>
                <div className="mb-3">
                    <label htmlFor="identificacionTextInput" className="form-label">Identificacion</label>
                    <input type="text" id="identificacionTextInput" className="form-control" placeholder="Identificacion" defaultValue={identificacion} readOnly />
                </div>
                <div className="mb-3">
                    <label htmlFor="fecha_nacimientoTextarea" className="form-label">Fecha de Nacimiento</label>
                    <textarea className="form-control" placeholder="Fecha de nacimiento" id="fecha_nacimientoTextarea" defaultValue={fecha_nacimiento.substring(0, 10)} readOnly></textarea>
                </div>
                
            </form>
            <button onClick={actualizarCliente} className="btn btn-dark">Actualizar</button>
                <span>    </span>
                <button onClick={eliminarCliente} className="btn btn-dark">Eliminar</button>
        </div>
    )
}
