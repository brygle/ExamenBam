import React, { useState } from 'react';

export const Reportes = ({ setCarnet }) => {

    const [inputValue, setInputValue] = useState('')
    
    const handleInputChange = (e) => {
        setInputValue(e.target.value);
    }

    const buscarCodigo = (e) => {
        e.preventDefault();
        setCarnet(inputValue);
        console.log(inputValue);
    }

    return (
        <div>
            <div className="input-group mb-3">
                <span className="input-group-text" id="basic-addon1">Código de cliente</span>
                <input 
                    type="text" 
                    className="form-control" 
                    placeholder="Código de cliente" 
                    aria-label="Código de cliente" 
                    aria-describedby="basic-addon1" 
                    value={ inputValue }
                    onChange={ handleInputChange }
                />
            </div>
            <button 
                className="btn btn-dark"
                onClick= { buscarCodigo }
            >
                Buscar
            </button>
            <hr />
        </div>
    )
}
