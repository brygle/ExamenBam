import { creds } from '../data/url';

export const createCliente = async (nombre1, nombre2, apellido1, apellido2, apellido_casada, direccion, telefono1, telefono2, identificacion, fecha_nacimiento) => {

    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(
            {
                nombre1: nombre1,
                nombre2: nombre2,
                apellido1: apellido1,
                apellido2: apellido2,
                apellido_casada: apellido_casada,
                direccion: direccion,
                telefono1: telefono1,
                telefono2: telefono2,
                identificacion: identificacion,
                fecha_nacimiento: fecha_nacimiento
            }
        )
    };
    let datos;
    console.log(creds['middleware'] + ':' + creds['port'] + '/api/cliente/crearCliente');
    await fetch(creds['middleware'] + ':' + creds['port'] +'/api/cliente/crearCliente', requestOptions)
        .then(response => response.json())
        .then(data => {
            console.log(data.msg);
            datos = data;
        }
        );
    return datos;
}