import { creds } from '../data/url';
export const deleteClienteByCodigo = async (cod) => {

    const requestOptions = {
        method: 'DELETE',
        headers: { 'Content-Type': 'application/json' },
    };
    let datos;
    console.log(creds['middleware'] + ':' + creds['port'] + '/api/cliente/eliminarCliente/' + cod);
    await fetch(creds['middleware'] + ':' + creds['port'] +'/api/cliente/eliminarCliente/' + cod, requestOptions)
        .then(response => response.json())
        .then(data => {
            console.log( data.msg );
            datos =  data;
        }
    );
    return datos;
}