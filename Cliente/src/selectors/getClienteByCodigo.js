import { creds } from '../data/url';
export const getClienteByCodigo = async (cod) => {

    const requestOptions = {
        method: 'GET',
        headers: { 
            'Content-Type': 'application/json' 
        },
    };
    let datos;
    console.log(creds['middleware'] + ':' + creds['port'] + '/api/cliente/verCliente/' + cod);
    await fetch(creds['middleware'] + ':' + creds['port'] +'/api/cliente/verCliente/' + cod, requestOptions)
        .then(response => response.json())
        .then(data => {
            datos =  data;
            console.log(datos);
        }
    );
    return datos;
}
