import { creds } from '../data/url';
export const getClientes = async () => {

    const requestOptions = {
        method: 'GET',
        headers: { 
            'Content-Type': 'application/json' 
        },
    };
    let datos;
    console.log(creds['middleware'] + ':' + creds['port'] + '/api/cliente/verClientes');
    await fetch(creds['middleware'] + ':' + creds['port'] +'/api/cliente/verClientes', requestOptions)
        .then(response => response.json())
        .then(data => {
            datos =  data;
            console.log(datos);
        }
    );
    return datos;
}
