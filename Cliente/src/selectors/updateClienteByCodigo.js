import { creds } from '../data/url';
export const updateClienteByCodigo = async (cod, dir, tel1) => {

    console.log('llego aqui')
    console.log(cod + " " + dir + " " + tel1);
    const requestOptions = {
        method: 'PUT',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(
            { 
                direccion: dir,
                telefono1: tel1
            }
        )
    };
    let datos;
    console.log(creds['middleware'] + ':' + creds['port'] + '/api/cliente/actualizarCliente/' + cod);
    await fetch(creds['middleware'] + ':' + creds['port'] +'/api/cliente/actualizarCliente/' + cod, requestOptions)
        .then(response => response.json())
        .then(data => {
            console.log( data.msg );
            datos =  data;
        }
    );
    console.log(datos);
    return datos;
}