CREATE DATABASE SEExamen2021;
USE SEExamen2021;
DROP DATABASE SEExamen2021;
CREATE TABLE Cliente(
	cli_codigo_cliente int not null identity(1,1) primary key , 
	cli_nombre1 varchar(40) not null,
	cli_nombre2 varchar(40),
	cli_apellido1 varchar(40) not null,
	cli_apellido2 varchar(40),
	cli_apellido_casada varchar(40),
	cli_direccion varchar(120) not null,
	cli_telefono1 int default 0,
	cli_telefono2 int default 0,
	cli_identificacion varchar(25) not null,
	cli_fecha_nacimiento smalldatetime not null
);
SELECT * FROM Cliente;